let handler = {
    clicked: false,
    paratext_section: null,
    svgContainer: null,
    data: {},
    places: {},
    ancients_names: {
        "Aristophanes": "Aristophanes",
        "Plautus": "Plautus",
        "Aeschylus": "Aeschylus",
        "Seneca": "Seneca",
        "Terentius": "Terentius",
        "Sophocles": "Sophocles",
        "Euripides": "Euripides"
    },
    zoom_level: 1,
    viewBox: {
        x: 80,
        y: 80,
        width: 200,
        height: 200
    },
    alignments: {
        "Tarvisii": "Tarvisii",
        "Venetiae": "Venetiae",
        "Londonii": "Londonii",
        "Lugduni": "Lugduni",
        "Geneuae": "Geneuae",
        "Basileae": "Basileae",
        "Brixiae": "Brixiae",
        "Argentorati": "Argentorati",
        "Haganoae": "Haganoae",
        "Barcinonae": "Barcinonae",
        "Lutetiae": "Lutetiae",
        "Heidelbergae": "Heidelbergae",
        "Francoforti ad Moenum": "Francoforti",
        "Londonii": "Londonii",
        "Mediolani": "Mediolani",
        "Lipsiae": "Lipsiae",
        "Vittebergae": "Vittebergae",
        "Rostochii": "Rostochii",
        "Venetiae": "Venetiae",
        "Prahae": "Prahae",
        "Coloniae": "Coloniae",
        "Florentiae": "Florentiae",
        "Romae": "Romae",
        "Parmae": "Parmae",
        "Augustae Vindelicorum": "Augustae Vindelicorum",
        "Nurembergae": "Nurembergae",
        "Alost": "Alost",
        "Louanii": "Louanii",
        "Lugduni": "Lugduni",
        "Antuerpiae": "Antuerpiae",
        "Lugduni Batauorum": "Lugduni Batauorum",
        "Vltraiecti": "Vltraiecti",
        "Parisiis": "Lutetiae",
        "Venetiis": "Venetiae",
        "Argentinae": "Argentinae",
        "Taruisii": "Tarvisii",
        "Aureliae Allobrogum": "Aureliae Allobrogum"
    },
    ancients_global: {
        "Aristophanes": 0,
        "Plautus": 0,
        "Aeschylus": 0,
        "Seneca": 0,
        "Terentius": 0,
        "Sophocles": 0,
        "Euripides": 0,
    },
    init: function (data) {
        this.paratext_section = document.getElementById("paratxt_list")
        this.svgContainer = document.getElementById("svg")
        let pubplaces_svg_elements = document.querySelectorAll("[pubplace]");
        for (var i = 0; i < pubplaces_svg_elements.length; i++) {
            new Place(pubplaces_svg_elements[i]);
        }
        handler.data = JSON.parse(data)
        chronos.init()
        document.getElementById("pubplace_focus_info").textContent = "Lieu de publication sélectionné ·········································· aucun";
        handler.zoomOut();
    },
    getAlignment: function (key) {
        if (this.alignments[key] !== undefined) {
            return this.alignments[key]
        }
        return key
    },
    getData: async function () {
        const options = {
            method: 'GET',
            mode: "cors",
        };
        return await fetch("https://ithac.elan-numerique.fr/api/paratexts-nested", options)
            .then((response) => {
                return response.text()
            })
    },
    generate_dots: function (n, glyph) {
        let str = '';
        for (var i = 0; i < n; i++) {
            str += glyph;
        }
        return str;
    },
    zoomOut: function () {
        if (handler.zoom_level < -15) return;
        handler.viewBox.x = handler.viewBox.x - handler.viewBox.width / 16;
        handler.viewBox.y = handler.viewBox.y - handler.viewBox.height / 16;
        handler.viewBox.width = handler.viewBox.width * 1.0625;
        handler.viewBox.height = handler.viewBox.height * 1.0625;
        handler.zoom_level--;
        handler.svgContainer.setAttributeNS(null, "handler.viewBox", handler.viewBox.x + ' ' + handler.viewBox.y + ' ' + handler.viewBox.width + ' ' + handler.viewBox.height);
    },
    selectPlace: function (place) {
        handler.resetAncientsGlobal();
        handler.updateAncientsLocal(place);
        handler.updateParatext();
        handler.updateTotalAncients();
        let text_length = ("Lieu de publication sélectionné" + handler.alignments[place.name]).length + 2;
        document.getElementById("pubplace_focus_info").textContent = "Lieu de publication sélectionné " + handler.generate_dots(80 - text_length, '·') + ' ' + handler.alignments[place.name];
    },
    updateHorizontalHistogram: function() {
        // pour chaque ancien, genere l'histo horizontal et met à jour le nombre de paratextes dans la vue.
        for (var ancient in handler.ancients_global) {
            let quantity = handler.generate_dots(Math.floor(handler.ancients_global[ancient] / 2 + 1), '#');
            let text_length = (handler.ancients_names[ancient] + '  ' + handler.ancients_global[ancient]).length + quantity.length + 1;
            document.getElementById(ancient + "_info").innerHTML = "<span>" + quantity + '</span> ' + handler.ancients_names[ancient] + ' ' + handler.generate_dots(80 - text_length, '·') + ' ' + handler.ancients_global[ancient];
        }
    },
    updateAncientsLocal: function (place) {
        // pour un lieu, et pour chaque ancient de ce lieu, compte le nombre de paratextes  
        for (var ancient in place.ancients) {
            handler.ancients_global[ancient] += place.ancients[ancient];
        }

        this.updateHorizontalHistogram()
    },
    updateAncientsGlobal: function () {
        // pour chaque lieu, et pour chaque ancient de ce lieu, compte le nombre de paratextes 
        // idem que updateAncientsLocal mais pour tous les lieux
        for (var place in handler.places) {
            for (var ancient in handler.places[place].ancients) {
                handler.ancients_global[ancient] += handler.places[place].ancients[ancient];
            }
        }
        this.updateHorizontalHistogram()
    },
    updateParatext: function () {
        // mise à jour du nombre de paratextes
        let nbParatexts = 0;
        for (var ancient in handler.ancients_global) {
            nbParatexts += handler.ancients_global[ancient];
        }
        let text_length = ('Paratextes  ' + nbParatexts).length;
        document.getElementById("paratxt_info").textContent = 'Paratextes ' + handler.generate_dots(80 - text_length, '·') + ' ' + nbParatexts;
    },
    updateTotalAncients: function () {
        let n_ancients = 0;
        for (var ancient in handler.ancients_global) {
            if (handler.ancients_global[ancient] == undefined || !handler.ancients_global[ancient]) continue;
            n_ancients += 1;
        }
        let text_length = ('Auteurs anciens cités ' + n_ancients).length + 1;
        document.getElementById("total_ancients_info").textContent = 'Auteurs anciens cités ' + handler.generate_dots(80 - text_length, '·') + ' ' + n_ancients;
    },
    resetAncientsGlobal: function () {
        for (var ancient in handler.ancients_global) {
            handler.ancients_global[ancient] = 0;
        }
    },
    reset_carto: function () {
        handler.resetAncientsGlobal();
        for (var place in handler.places) {
            for (var ancient in handler.places[place].ancients) {
                handler.places[place].ancients[ancient] = 0;
                handler.places[place].updateSize(1);
            }
        }
    },
    create_paratextes_cards: function (place, year, place_name) {
        for (var ancient in place) {
            for (var title in place[ancient]) {
                let filename = Object.values(place[ancient][title])[0]
                let card = document.createElement("div");
                card.addEventListener("mousedown", function () {
                    window.open("https://ithac.elan-numerique.fr/p/" + filename);
                });
                card.classList.add("paratxt_card");
                card.setAttribute("place", place_name);
                let date_card = document.createElement("p");
                var text_length = ("Date" + year).length + 2;
                date_card.textContent = "Date " + handler.generate_dots(80 - text_length, '·') + ' ' + year;
                card.appendChild(date_card);

                let title_card = document.createElement("p");
                text_length = ("Titre " + title).length + 2;
                title_card.textContent = "Titre ·" + handler.generate_dots(80 - text_length, '·') + ' ' + title;
                card.appendChild(title_card);

                let author_card = document.createElement("p");
                let authorName = handler.getAlignment(Object.keys(place[ancient][title])[0])
                text_length = ("Author " + authorName).length + 2;
                author_card.textContent = "Author ·" + handler.generate_dots(80 - text_length, '·') + ' ' + authorName;
                card.appendChild(author_card);

                let place_card = document.createElement("p");
                let placeName = handler.getAlignment(place_name)
                text_length = ("Place " + placeName).length + 2;
                place_card.textContent = "Place ·" + handler.generate_dots(80 - text_length, '·') + ' ' + placeName;
                card.appendChild(place_card);

                let ancient_card = document.createElement("p");
                let ancientName = handler.getAlignment(ancient)
                text_length = ("Auteur antique " + ancientName).length + 2;
                ancient_card.textContent = "Auteur antique ·" + handler.generate_dots(80 - text_length, '·') + ' ' + ancientName;
                card.appendChild(ancient_card);
                handler.paratext_section.appendChild(card);
            }
        }
    },
    print_years_on_map: function (years) {
        handler.reset_carto();
        let text_length = ("Période sélectionnée" + (years.length > 1 ? (years[0] + ' - ' + years[years.length - 1]) : years[0])).length + 2;
        document.getElementById("period_info").textContent = "Période sélectionnée " + handler.generate_dots(80 - text_length, '·') + ' ' + (years.length > 1 ? (years[0] + ' - ' + years[years.length - 1]) : years[0]);
        text_length = ("Années" + years.length).length + 2;
        document.getElementById("years_info").textContent = "Années " + handler.generate_dots(80 - text_length, '·') + ' ' + years.length;
        let pubplaces = {};
        let pubplaces_number = 0;
        while (handler.paratext_section.children.length) handler.paratext_section.removeChild(handler.paratext_section.children[0]);
        while (years.length) {
            let year = years.pop();
            let places_data = handler.data[year];
            if (places_data == undefined) continue;
            for (var place in places_data) {
                if (!pubplaces[place]) {
                    pubplaces[place] = true;
                    pubplaces_number++;
                }
                handler.create_paratextes_cards(places_data[place], year, place);
                let total_ancients = handler.get_all_child_total_from_data(places_data[place]);
                for (var ancient in places_data[place]) {
                    if (handler.places[place] == undefined) continue;
                    handler.places[place].ancients[ancient] += total_ancients[ancient];
                    handler.places[place].updateSize(0.15);
                }
            }
        }
        text_length = ("Lieux de publication" + pubplaces_number).length + 2;
        document.getElementById("pubplace_info").textContent = "Lieux de publication " + handler.generate_dots(80 - text_length, '·') + ' ' + pubplaces_number;
        if (handler.clicked) {
            handler.updateAncientsLocal(handler.places[handler.clicked]);
            let cards = document.getElementsByClassName("paratxt_card");

            for (var j = 0; j < cards.length; j++) {
                if (cards[j].getAttribute("place") == handler.clicked) continue;
                cards[j].style.display = "none";
            }
        } else {
            handler.updateAncientsGlobal();
        }
        handler.updateTotalAncients();
        handler.updateParatext();
    },
    make_list_from_range: function (from, to) {
        let list = [];
        let interval = to - from;
        while (interval--) {
            list.push(from + interval);
        }
        list.reverse();
        list.push(to);
        return list;
    },
    get_total_from_data: function (data, total) {
        if (data instanceof Object) {
            for (var key in data) {
                if (Object.keys(data[key]).length) {
                    handler.get_total_from_data(data[key], total);
                }
            }
        } else {
            total.result += 1
        }
    },
    get_all_child_total_from_data: function (data) {
        let results = {};
        for (var i in data) {
            let total = { "result": 0 };
            handler.get_total_from_data(data[i], total);
            results[i] = total.result;
        }
        return results;
    }
}

/***
 * 
 * PLACE
 * 
 */

Place = function (svg_el) {
    this.name = svg_el.getAttribute("pubplace");

    this.ancients = {
        "Aristophanes": 0,
        "Plautus": 0,
        "Aeschylus": 0,
        "Seneca": 0,
        "Terentius": 0,
        "Sophocles": 0,
        "Euripides": 0,
    }
    this.svg_el = svg_el;
    this.ancients_elements = {};
    this.setup_ancients_circles();
    handler.places[this.name] = this;
}
Place.prototype.setup_ancients_circles = function () {
    this.pos_x = this.svg_el.getAttribute("cx");
    this.pos_y = this.svg_el.getAttribute("cy");
    this.text = document.createElementNS('http://www.w3.org/2000/svg', 'text');

    let order = [];
    for (var ancient in this.ancients) {

        this.ancients_elements[ancient] = document.createElementNS("http://www.w3.org/2000/svg", "circle");
        this.ancients_elements[ancient].place = this;
        this.ancients_elements[ancient].setAttributeNS(null, "cx", this.pos_x);
        this.ancients_elements[ancient].setAttributeNS(null, "cy", this.pos_y);
        this.ancients_elements[ancient].setAttributeNS(null, "r", '0px');
        this.ancients_elements[ancient].setAttributeNS(null, "ancient", ancient);
        this.ancients_elements[ancient].setAttributeNS(null, "ancient_circle", this.name + '_' + ancient);
        this.ancients_elements[ancient].linked_text = this.text;
        this.ancients_elements[ancient].addEventListener('mouseenter', function () {
            if (handler.clicked != this.place.name) {
                handler.clicked = false;
                let circle_count = document.getElementsByClassName("text_count_circle");
                for (var i in handler.places) {
                    for (var ancient in handler.places[i].ancients) {
                        event = document.createEvent("HTMLEvents");
                        event.initEvent("mouseout", true, true);
                        event.e; ventName = "mouseout";
                        if (!handler.places[i].ancients_elements[ancient]) break;
                        handler.places[i].ancients_elements[ancient].dispatchEvent(event);
                    }
                    for (var j = 0; j < circle_count.length; j++) {
                        if (circle_count[j].getAttribute("place") == this.place.name) continue;
                        circle_count[j].style.display = "none";
                    }
                }
            }
            let cards = document.getElementsByClassName("paratxt_card");
            let circle_count = document.getElementsByClassName("text_count_circle");
            this.linked_text.style.opacity = '1';
            for (var i in handler.places) {
                for (var j = 0; j < cards.length; j++) {
                    if (cards[j].getAttribute("place") == this.place.name) continue;
                    cards[j].style.display = "none";
                }
                for (var j = 0; j < cards.length; j++) {
                    if (cards[j].getAttribute("place") == this.place.name) continue;
                    cards[j].style.display = "none";
                }
                for (var j in handler.places[i].ancients_elements) {
                    if (handler.places[i] == this.place) continue;
                    handler.places[i].ancients_elements[j].style.opacity = 0.1;
                }
            }
            handler.selectPlace(this.place);
        });
        this.ancients_elements[ancient].addEventListener('mousedown', function () {
            for (var i in handler.places) {
                for (var ancient in handler.places[i].ancients) {
                    event = document.createEvent("HTMLEvents");
                    event.initEvent("mouseout", true, true);
                    event.e; ventName = "mouseout";
                    if (!handler.places[i].ancients_elements[ancient]) break;
                    handler.places[i].ancients_elements[ancient].dispatchEvent(event);
                }
            }
            let circle_count = document.getElementsByClassName("text_count_circle");
            let cards = document.getElementsByClassName("paratxt_card");
            this.linked_text.style.opacity = '1';
            for (var i in handler.places) {
                for (var j = 0; j < cards.length; j++) {
                    if (cards[j].getAttribute("place") == this.place.name) continue;
                    cards[j].style.display = "none";
                }
                for (var j = 0; j < circle_count.length; j++) {
                    if (circle_count[j].getAttribute("place") == this.place.name) continue;
                    circle_count[j].style.display = "none";
                }
                for (var j in handler.places[i].ancients_elements) {
                    if (handler.places[i] == this.place) continue;
                    handler.places[i].ancients_elements[j].style.opacity = 0.1;
                }
            }
            handler.selectPlace(this.place);
            if (handler.clicked == this.place.name) {
                handler.clicked = false;
            } else {
                handler.clicked = this.place.name;
            }
        });
        this.ancients_elements[ancient].addEventListener('mouseout', function () {
            if (handler.clicked) return;
            let cards = document.getElementsByClassName("paratxt_card");
            let circle_count = document.getElementsByClassName("text_count_circle");
            this.linked_text.style.opacity = '0';
            for (var j = 0; j < cards.length; j++) {
                cards[j].style.display = "block";
            }
            for (var j = 0; j < circle_count.length; j++) {
                circle_count[j].style.display = "block";
            }
            for (var i in handler.places) {
                for (var j in handler.places[i].ancients_elements) {
                    handler.places[i].ancients_elements[j].style.opacity = 1;
                }
            }
            handler.resetAncientsGlobal();
            handler.updateAncientsGlobal();
            handler.updateParatext();
            handler.updateTotalAncients();
            document.getElementById("pubplace_focus_info").textContent = "Lieu de publication sélectionné ·········································· aucun";
        });
        order.push(this.ancients_elements[ancient]);
    }

    order.reverse();
    for (var i = 0; i < order.length; i++) {
        handler.svgContainer.appendChild(order[i]);
    }

    this.text.setAttribute('x', this.pos_x);
    this.text.setAttribute('y', this.pos_y);
    this.text.textContent = handler.alignments[this.name];
    this.text.style.opacity = '0';
    handler.svgContainer.appendChild(this.text);
}
Place.prototype.updateSize = function (ratio) {
    let previous_size = 0;
    let base = 6;
    for (var ancient in this.ancients) {
        if (this.ancients_elements[ancient] == undefined) continue;
        let size = !this.ancients[ancient] ? 0 : this.ancients[ancient] < base ? base : this.ancients[ancient];
        this.ancients_elements[ancient].setAttributeNS(null, "r", (size + previous_size) * ratio);
        previous_size += size;
    }
    this.text.setAttribute('x', parseInt(this.pos_x) + previous_size * ratio + 2);
}

/***
* 
* CHRONOS
* 
*/
let chronos = {
    chrono_thumb_A_clicked: false,
    chrono_thumb_B_clicked: false,
    chrono_thumb_A_min_x: 0,
    chrono_thumb_B_min_x: 0,
    chrono_thumb_A_max_x: 0,
    chrono_thumb_B_max_x: 0,
    chrono_thumb_A_x: 0,
    chrono_thumb_B_x: 0,
    chrono_thumb_A: null,
    chrono_thumb_B: null,
    frame: 0,
    mouse_x: 0,

    init: function () {
        this.chrono_thumb_A = document.createElement("div");
        this.chrono_thumb_A.classList.add("chrono_thumb");
        this.chrono_thumb_A.classList.add("chrono_thumb_A");
        this.chrono_thumb_B = document.createElement("div");
        this.chrono_thumb_B.classList.add("chrono_thumb");
        this.chrono_thumb_B.classList.add("chrono_thumb_B");
        this.chrono_thumb_A.addEventListener("mousedown", function () {
            event.preventDefault();
            chronos.chrono_thumb_A_clicked = true;
        });
        this.chrono_thumb_B.addEventListener("mousedown", function () {
            event.preventDefault();
            chronos.chrono_thumb_B_clicked = true;
        });

        document.addEventListener("mousemove", function (e) {
            chronos.mouse_x = e.clientX;
        });

        document.addEventListener("mouseup", function () {
            chronos.chrono_thumb_A_clicked = false;
            chronos.chrono_thumb_B_clicked = false;
        });

        this.build_chrono_ui(1460, 1640);

        chronos.chrono_thumb_B_x = chronos.chrono_thumb_B_max_x - 8;
    },

    build_chrono_ui: function (chrono_min_date, chrono_max_date) {
        let chrono_bar = document.createElement("div");
        chrono_bar.classList.add("chrono_bar");
        chrono_bar.appendChild(chronos.chrono_thumb_A);
        chrono_bar.appendChild(chronos.chrono_thumb_B);
        let chrono_graduation = document.createElement("div");
        chrono_graduation.classList.add('chrono_graduation');
        let chrono_texts = document.createElement("div");
        chrono_texts.classList.add('chrono_texts');
        chrono_graduation.classList.add('chrono_graduation');
        chrono_ui_section = document.getElementById("chrono_ui");
        map_side_section = document.getElementById("map_side");
        histogram_section = document.getElementById("histogram_section");

        // Infos
        let info_ancients = document.createElement("p");
        info_ancients.id = "info_ancients";
        info_ancients.style.marginBottom = "1rem";
        info_ancients.textContent = "------------------------------------ INFOS -------------------------------------";
        chrono_ui_section.appendChild(info_ancients);

        let period_info = document.createElement("p");
        period_info.id = "period_info";
        period_info.textContent = "Période sélectionnée :";
        chrono_ui_section.appendChild(period_info);

        let years_info = document.createElement("p");
        years_info.id = "years_info";
        years_info.textContent = "Années :";
        chrono_ui_section.appendChild(years_info);

        let pubplace_info = document.createElement("p");
        pubplace_info.id = "pubplace_info";
        pubplace_info.textContent = "Lieux de publication :";
        chrono_ui_section.appendChild(pubplace_info);

        let pubplace_focus_info = document.createElement("p");
        pubplace_focus_info.id = "pubplace_focus_info";
        pubplace_focus_info.textContent = "Lieu de publication sélectionné :";
        chrono_ui_section.appendChild(pubplace_focus_info);

        let total_ancients_info = document.createElement("p");
        total_ancients_info.id = "total_ancients_info";
        total_ancients_info.textContent = "Auteurs anciens cités :";
        chrono_ui_section.appendChild(total_ancients_info);

        let detail_ancients = document.createElement("p");
        detail_ancients.style.marginTop = "1rem";
        detail_ancients.style.marginBottom = "1rem";
        detail_ancients.id = "detail_ancients";
        detail_ancients.textContent = "------------------------------------ Détail ------------------------------------";
        chrono_ui_section.appendChild(detail_ancients);

        let ar_info = document.createElement("p");
        ar_info.classList.add("ancient-name")
        ar_info.id = "Aristophanes_info";
        ar_info.dataset.ancient = "Aristophanes"
        ar_info.textContent = "Aristophanes :";
        chrono_ui_section.appendChild(ar_info);

        let pl_info = document.createElement("p");
        pl_info.classList.add("ancient-name")
        pl_info.id = "Plautus_info";
        pl_info.dataset.ancient = "Plautus"
        pl_info.textContent = "Plautus :";
        chrono_ui_section.appendChild(pl_info);


        let eschl_info = document.createElement("p");
        eschl_info.classList.add("ancient-name")
        eschl_info.id = "Aeschylus_info";
        eschl_info.dataset.ancient = "Aeschylus"
        eschl_info.textContent = "Aeschylus :";
        chrono_ui_section.appendChild(eschl_info);

        let sen_info = document.createElement("p");
        sen_info.classList.add("ancient-name")
        sen_info.id = "Seneca_info";
        sen_info.dataset.ancient = "Seneca"
        sen_info.textContent = "Seneca :";
        chrono_ui_section.appendChild(sen_info);

        let ter_info = document.createElement("p");
        ter_info.classList.add("ancient-name")
        ter_info.id = "Terentius_info";
        ter_info.dataset.ancient = "Terentius"
        ter_info.textContent = "Terentius :";
        chrono_ui_section.appendChild(ter_info);

        let soph_info = document.createElement("p");
        soph_info.classList.add("ancient-name")
        soph_info.id = "Sophocles_info";
        soph_info.dataset.ancient = "Sophocles"
        soph_info.textContent = "Sophocles :";
        chrono_ui_section.appendChild(soph_info);

        let eur_info = document.createElement("p");
        eur_info.classList.add("ancient-name")
        eur_info.id = "Euripides_info";
        eur_info.dataset.ancient = "Euripides"
        eur_info.textContent = "Euripides :";
        chrono_ui_section.appendChild(eur_info);

        let total_ancients = document.createElement("p");
        total_ancients.classList.add("ancient-name")
        total_ancients.style.marginTop = "1rem";
        total_ancients.style.marginBottom = "1rem";
        total_ancients.id = "total_ancients";
        total_ancients.textContent = "------------------------------------ Total -------------------------------------";
        chrono_ui_section.appendChild(total_ancients);

        let paratxt_info = document.createElement("p");
        paratxt_info.id = "paratxt_info";
        paratxt_info.textContent = "Paratextes :";
        chrono_ui_section.appendChild(paratxt_info);


        // HISTOGRAM

        histogram_section.appendChild(chrono_texts);
        histogram_section.appendChild(chrono_graduation);
        histogram_section.appendChild(chrono_bar);
        this.chrono_thumbs_handling(chrono_min_date, chrono_max_date);
        for (var i = 0; i < chrono_max_date - chrono_min_date - 1; i++) {
            let text_count = document.createElement('div');
            text_count.classList.add("text_count");
            for (var j in handler.data[chrono_min_date + i]) {
                let text_count_circle = document.createElement('div');
                text_count_circle.classList.add("text_count_circle");
                text_count_circle.classList.add("text_count_" + (chrono_min_date + i));
                let circle_size = 0;
                circle_size = handler.data[chrono_min_date + i][j];
                let size = handler.get_all_child_total_from_data({ circle_size }).circle_size;
                text_count_circle.setAttribute("quantity", size);
                text_count_circle.setAttribute("place", j);
                text_count_circle.addEventListener("mouseenter", function () {
                    text_count_circle.style.overflow = "visible";
                    // handler.alignments est déclaré dans common/handler.alignments.js
                    text_count_circle.textContent = '_' + handler.alignments[this.getAttribute("place")] + ':' + this.getAttribute("quantity");
                });
                text_count_circle.addEventListener("mouseout", function () {
                    text_count_circle.style.overflow = "hidden";
                    text_count_circle.textContent = "";
                });
                text_count_circle.style.width = '1vw';
                text_count_circle.style.height = (size * 5 + 8) + 'px';
                if (circle_size) text_count.appendChild(text_count_circle);
            }
            text_count.style.width = (chronos.chrono_thumb_A_max_x / (chrono_max_date - chrono_min_date)) + 'px';
            chrono_texts.appendChild(text_count);
            if (Number.isInteger(i / 5)) {
                let mark = document.createElement('div');
                mark.classList.add('chrono_mark');
                if (Number.isInteger(i / 20)) {
                    let content = document.createElement('p');
                    content.textContent = '' + (chrono_min_date + i);
                    mark.appendChild(content);
                    mark.style.height = '2.5rem';
                } else {
                    mark.style.marginTop = '1.5rem';
                    mark.style.height = '1rem';
                }
                mark.style.width = (chronos.chrono_thumb_A_max_x / (chrono_max_date - chrono_min_date)) * 5 + 'px';
                chrono_graduation.appendChild(mark);
            }
        }
        handler.print_years_on_map(handler.make_list_from_range(chrono_min_date, chrono_max_date));
    },

    chrono_thumbs_handling: function (chrono_min_date, chrono_max_date) {
        // UI
        chronos.chrono_thumb_A_min_x = chronos.chrono_thumb_A.parentNode.getBoundingClientRect().x;
        chronos.chrono_thumb_A_max_x = chronos.chrono_thumb_A.parentNode.getBoundingClientRect().width;
        chronos.chrono_thumb_B_min_x = chronos.chrono_thumb_A.parentNode.getBoundingClientRect().x;
        chronos.chrono_thumb_B_max_x = chronos.chrono_thumb_A.parentNode.getBoundingClientRect().width;
        if (chronos.chrono_thumb_A_clicked) {
            pos_x = this.mouse_x - chronos.chrono_thumb_A_min_x;
            chronos.chrono_thumb_A_x = pos_x > 0 && pos_x < chronos.chrono_thumb_A_max_x ? pos_x : chronos.chrono_thumb_A_x;
            chronos.chrono_thumb_B_x = chronos.chrono_thumb_A_x > chronos.chrono_thumb_B_x ? chronos.chrono_thumb_A_x : chronos.chrono_thumb_B_x;
        }
        if (chronos.chrono_thumb_B_clicked) {
            pos_x = this.mouse_x - chronos.chrono_thumb_B_min_x;
            chronos.chrono_thumb_B_x = pos_x > 0 && pos_x < chronos.chrono_thumb_B_max_x - 8 ? pos_x : chronos.chrono_thumb_B_x;
            chronos.chrono_thumb_A_x = chronos.chrono_thumb_B_x < chronos.chrono_thumb_A_x ? chronos.chrono_thumb_B_x : chronos.chrono_thumb_A_x;
        }
        chronos.chrono_thumb_A.style.marginLeft = "calc(" + chronos.chrono_thumb_A_x + "px - 0.5rem)";
        chronos.chrono_thumb_B.style.marginLeft = "calc(" + (chronos.chrono_thumb_B_x - chronos.chrono_thumb_A_x) + "px - 1rem)";
        // DATES
        if (chronos.chrono_thumb_A_clicked || chronos.chrono_thumb_B_clicked) {
            let chrono_min_range = Math.floor(chrono_min_date + (chronos.chrono_thumb_A_x / (chronos.chrono_thumb_B_max_x / (chrono_max_date - chrono_min_date))));
            let chrono_max_range = Math.floor(chrono_min_date + (chronos.chrono_thumb_B_x / (chronos.chrono_thumb_B_max_x / (chrono_max_date - chrono_min_date))));
            let textcount_circles = document.getElementsByClassName("text_count_circle");
            for (var i = 0; i < textcount_circles.length; i++) {
                textcount_circles[i].style.borderColor = "var(--color-c)";
            }
            for (var i = chrono_min_date; i < chrono_min_range; i++) {
                let textcount = document.getElementsByClassName("text_count_" + i);
                if (textcount) {
                    for (var j = 0; j < textcount.length; j++) {
                        textcount[j].style.borderColor = "var(--color-d)";
                    }
                }
            }
            for (var i = chrono_max_range + 1; i < chrono_max_date; i++) {
                let textcount = document.getElementsByClassName("text_count_" + i);
                if (textcount) {
                    for (var j = 0; j < textcount.length; j++) {
                        textcount[j].style.borderColor = "var(--color-d)";
                    }
                }
            }
            handler.print_years_on_map(handler.make_list_from_range(chrono_min_range, chrono_max_range));
        }
        this.frame++;
        setTimeout(function () { chronos.chrono_thumbs_handling(chrono_min_date, chrono_max_date) }, 17 * 2);
    }
}

/***
* 
* DOCUMENT LOAD
* 
*/
window.addEventListener('load', function () {
    handler.getData().then(data => {
        handler.init(data)
    });
})