document.addEventListener("DOMContentLoaded", function() {
  noteHandler.init();
});

var noteHandler = {
  bgClass:"note-hover",

  init: function(){
    let notes = document.querySelectorAll("sup[data-type=note]")
    notes.forEach(function(note) {
      note.addEventListener("mouseenter", noteHandler.addBackground, false)
      note.addEventListener("mouseout", noteHandler.removeBackground, false)
    });

    let noteCalls = document.querySelectorAll("sup[data-type=note]>a")
    noteCalls.forEach(function(noteCall) {
      noteCall.addEventListener("click", noteHandler.modal, false)
    });


    let noteLinks = document.querySelectorAll("a[data-type=note-link]")
    noteLinks.forEach(function(noteLink) {
      noteLink.addEventListener("click", noteHandler.backToNoteCall, false)
    });

    return;
  },

  scrollToEl: function(el) {
    el.scrollIntoView()
    window.scroll(0, window.scrollY - 100)
  },

  backToNoteCall: function(evt){
    evt.preventDefault()

    let noteLink = evt.currentTarget
    let noteId = noteLink.getAttribute('href')
    noteId = noteId.substring(1);
    let note = document.querySelector("[name="+noteId+"]")
    let seg = noteHandler.getSeg(note.parentNode)
    seg.classList.add(noteHandler.bgClass)
    setTimeout(() => {seg.classList.remove(noteHandler.bgClass)}, 4000);
    noteHandler.scrollToEl(seg)

    return;
  },

  modal: function(evt) {
    evt.preventDefault()
    const sel = '#note-modal'
    const modalBody = document.querySelector(sel+" .modal-body")
    const target = evt.target.getAttribute("href").substring(1)
    let node = document.querySelector("[name="+target+"]").parentNode.cloneNode(true)

    // remove note number
    node.children[0].replaceWith(node.children[0].innerText +  " ");

    // remove annotate btn
    let annotate = node.querySelector('.annotate')
    if(annotate) {
      node.querySelector('.annotate').remove()
    }

    modalBody.innerHTML = node.innerHTML
    $(sel).modal("show")
  },
  
  addBackground: function(evt){
    let seg = noteHandler.getSeg(evt.currentTarget)
    if(seg) {
      seg.classList.add(noteHandler.bgClass)
    }

    return;
  },

  removeBackground: function(evt){
    let seg = noteHandler.getSeg(evt.currentTarget)
    if(seg) {
      seg.classList.remove(noteHandler.bgClass)
    }

    return;
  },

  getSeg: function(note){
    let target = note.dataset.ref

    return seg = document.querySelector("span[data-type=segment][data-id="+target+"]")
  }
}
