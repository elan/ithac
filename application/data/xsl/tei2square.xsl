<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:output method="html" indent="yes"/>
    
    <xsl:variable name="folder">
        <xsl:text>public/upload/tei/</xsl:text>
    </xsl:variable>
    <xsl:variable name="files" select="collection(concat('../../', $folder, '?select=*.xml'))"/>

    <!--<xsl:param name="level">author</xsl:param>-->
    <!--<xsl:param name="level">edition</xsl:param>-->
    <!--<xsl:param name="level">paratext</xsl:param>-->
    <xsl:param name="level">paratext</xsl:param>
    <xsl:param name="author">aristophanes</xsl:param>
    <xsl:param name="edition">1501</xsl:param>

    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="$level = 'author'">
                <xsl:apply-templates mode="author"/>
            </xsl:when>
            <xsl:when test="$level = 'edition'">
                <xsl:apply-templates mode="edition">
                    <xsl:with-param name="author">
                        <xsl:value-of select="$author"/>
                    </xsl:with-param>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:when test="$level = 'paratext'">
                <xsl:apply-templates mode="paratext">
                    <xsl:with-param name="author" select="$author"/>
                    <xsl:with-param name="edition" select="$edition"/>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:otherwise>
                <p>
                    <xsl:text>Niveau d'information </xsl:text>
                    <code>
                        <xsl:value-of select="$level"/>
                    </code>
                    <xsl:text> inconnu</xsl:text>
                </p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="/tei:TEI" mode="author">
        <xsl:for-each-group
            select="$files//tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl/tei:author"
            group-by=".">
            <xsl:sort select="current-grouping-key()"/>
            <xsl:variable name="author" select="normalize-space(lower-case(.))"/>
            <xsl:if test="string-length($author) > 0">
                <div class="col mb-4">
                    <div class="card h-100 color-block">
                        <div class="card-body">
                            <h5 class="card-title">
                                <xsl:value-of select="."/>
                            </h5>
                            <p class="card-text">
                                <xsl:value-of select="count(current-group())"/>
                                <xsl:text> édition</xsl:text>
                                <xsl:if test="count(current-group()) > 1">s</xsl:if>
                            </p>
                            <a class="btn btn-sm btn-secondary card-link float-right"
                                href="/admin/author/{$author}"> Afficher </a>
                        </div>
                    </div>
                </div>
            </xsl:if>
        </xsl:for-each-group>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="edition">
        <xsl:param name="author"/>
        <xsl:for-each-group
            select="$files//tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl[normalize-space(lower-case(tei:author)) = $author]/tei:date/@when"
            group-by=".">
            <xsl:sort select="current-grouping-key()"/>
            <div class="col mb-4">
                <div class="card h-100 color-block">
                    <div class="card-body">
                        <h5 class="card-title">
                            <xsl:value-of select="."/>
                        </h5>
                        <p class="card-text">
                            <xsl:value-of select="count(current-group())"/>
                            <xsl:text> paratexte</xsl:text>
                            <xsl:if test="count(current-group()) > 1">s</xsl:if>
                        </p>
                        <a class="btn btn-sm btn-secondary card-link float-right"
                            href="/admin/edition/{$author}/{.}"> Afficher </a>
                    </div>
                </div>
            </div>
        </xsl:for-each-group>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="paratext">
        <xsl:param name="author"/>
        <xsl:param name="edition"/>
        <xsl:for-each
            select="$files[tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:bibl[normalize-space(lower-case(tei:author)) = $author]/tei:date/@when = $edition]">
            <xsl:sort select="substring-before(substring-after(document-uri(.), $folder), '-')"/>
            <xsl:variable name="idParatext"
                select="substring-before(substring-after(document-uri(.), $folder), '-')"/>
            <xsl:if test="string-length($author) > 0">
                <div class="col mb-4">
                    <div class="card h-100 color-block">
                        <div class="card-body">
                            <h5 class="card-title">
                                <xsl:value-of
                                    select="./tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"
                                />
                            </h5>
                            <h6>
                                <xsl:value-of select="$idParatext"/>
                            </h6>
                            <a class="btn btn-sm btn-secondary card-link float-right"
                                href="/admin/paratext/{$idParatext}"> Afficher </a>
                        </div>
                    </div>
                </div>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
