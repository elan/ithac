<?php

namespace App\Controller;

use App\Manager\MailManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        // $this->addFlash('notice', 'Toast flashmessages');

        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/team", name="team")
     */
    public function team()
    {
        return $this->render('editorial/team.html.twig');
    }

    /**
     * @Route("/mode-emploi", name="mode_emploi")
     */
    public function modeEmploi()
    {
        return $this->render('editorial/mode-emploi.html.twig');
    }

    /**
     * @Route("/mention-legales", name="legal_notices")
     */
    public function legalNotices()
    {
        return $this->render('default/legal-notices.html.twig');
    }

    /**
     * @Route("/manual", name="manual")
     */
    public function manual()
    {
        return $this->render('editorial/manual.html.twig');
    }

    /**
     * @Route("/edito-note", name="edito_note")
     */
    public function editorialNote()
    {
        return $this->render('editorial/edito-notes.html.twig');
    }

    /**
     * @Route("/realisations", name="realisations")
     */
    public function realisations()
    {
        return $this->render('editorial/realisations.html.twig');
    }

    /**
     * @Route("/bibliographie", name="bibliographie")
     */
    public function bibliographie()
    {
        return $this->render('editorial/bibliographie.html.twig');
    }

    
    /**
     * @Route("/test-mail", name="test_mail")
     */
    public function mail(MailManager $mm)
    {
        $mm->sendGenericMail();

        return $this->render('default/index.html.twig');
    }
}
