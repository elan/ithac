<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Edition;
use App\Entity\Author;

/** @Route(name="edition_") */
class EditionController extends AbstractController
{
    /**
     * @Route("/edition/{id}", name="display")
     * @Route("/e/{author}/{name}", name="display_long")
     */
    public function display($id = null, $author = null, $name = null)
    {
        if ($id) {
            $edition = $this->getDoctrine()->getRepository(Edition::class)->find($id);
        } else {
            $author = $this->getDoctrine()->getRepository(Author::class)->findOneBy(["name" => $author]);
            $edition = $this->getDoctrine()->getRepository(Edition::class)->findOneBy(["name" => $name, "author" => $author->getId()]);
        }

        return $this->render('edition/display.html.twig', ['edition' => $edition]);
    }

    /**
     * @Route("/admin/edition/{id}/delete", name="remove")
     */
    public function delete(Edition $edition)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');

        $author = $edition->getAuthor();

        $em = $this->getDoctrine()->getManager();
        $em->remove($edition);
        $em->flush();

        $this->addFlash('notice', 'Edition supprimée');

        return $this->redirectToRoute('author_display', ["id" => $author->getId()]);
    }
}
