<?php

namespace App\Controller;

use App\Entity\Paratext;
use App\Manager\VisualisationManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class VisualisationController extends AbstractController
{
    /**
     * @Route("/explorer", name="explorer")
     */
    public function explorer()
    {

        return $this->render('visualisation/explorer.html.twig');
    }

    /**
     * @Route("/carte", name="carte")
     */
    public function carte()
    {

        return $this->render('visualisation/carte.html.twig');
    }

    /**
     * @Route("/api/paratexts", name="paratexts", options={"expose"=true})
     */
    public function paratexts()
    {
        $em = $this->getDoctrine()->getManager();
        $paratexts = $em->getRepository(Paratext::class)->findAll();
        $paratextsFiles = [];
        foreach ($paratexts as $paratext) {
            $paratextsFiles[] = $paratext->getFilename();
        }

        return new JsonResponse($paratextsFiles);
    }

    /**
     * @Route("/api/paratexts-nested", name="paratexts-nested", options={"expose"=true})
     */
    public function paratextsNested(VisualisationManager $visManager)
    {
        $paratextsExtended = $visManager->getParatextsExtended();
        $nested = $visManager->extendedToNested($paratextsExtended);

        return new JsonResponse($nested);
    }

    /**
     * @Route("/api/paratexts-extended", name="paratexts-extended", options={"expose"=true})
     */
    public function paratextsExtended(VisualisationManager $visManager)
    {
        $paratextsExtended = $visManager->getParatextsExtended();

        return new JsonResponse($paratextsExtended);
    }

    /**
     * @Route("/api/paratext/{filename}", name="paratext", options={"expose"=true})
     */
    public function paratext($filename)
    {
        $dir = $this->getParameter('xml_directory');
        $fileContent = file_get_contents($dir . DIRECTORY_SEPARATOR . $filename);

        return new Response($fileContent);
    }
}
