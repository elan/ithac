<?php

namespace App\Manager;

use App\Entity\Author;
use Doctrine\ORM\EntityManagerInterface;

class AuthorManager
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function create($name)
    {
        $author = new Author();
        $author->setName($name);

        return $author;
    }

    public function save(Author $author)
    {
        $this->em->persist($author);
        $this->em->flush();

        return;
    }

    public function cleanEmpty()
    {
        $emptyAuthors = $this->em->getRepository(Author::class)->findEmpty();
        foreach ($emptyAuthors as $emptyAuthor) {
            $this->em->remove($emptyAuthor);
        }
        $this->em->flush();

        return;
    }
}
