<?php

namespace App\Manager;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailManager
{
    private $mailer;
    private $templating;
    private $params;
    public function __construct(ParameterBagInterface $params, MailerInterface $mailer, \Twig_Environment $templating)
    {
        $this->params = $params;
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function sendGenericMail()
    {
        $body = $this->templating->render(
          'mail/mail.html.twig',
          ['body' => "contenu test"]
        );

        $to = "";
        $subject= "test template";

        $this->sendEmail($to, $subject, $body);
    }

    public function sendEmail($to, $subject, $body)
    {
        $email = (new Email())
            ->from($this->params->get('platform_email'))
            ->to($to)
            ->subject($subject)
            ->html($body);

        $this->mailer->send($email);

      return;
    }
}
