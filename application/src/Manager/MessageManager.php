<?php

namespace App\Manager;

use App\Entity\Message;
use App\Entity\Paratext;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Security;

class MessageManager
{
    public function __construct(EntityManagerInterface $em, Security $security)
    {
        $this->em = $em;
        $this->user = $security->getUser();
    }

    public function create(Message $message, Paratext $paratext)
    {
        $message->setParatext($paratext);
        $message->setCreationDate(new \DateTime());
        $message->setSender($this->user);

        $this->saveMessage($message);

        return $message;
    }

    public function saveMessage(Message $message)
    {
        $this->em->persist($message);
        $this->em->flush();
    }

    public function remove(Message $message)
    {
        $this->em->remove($message);
        $this->em->flush();
    }
}
