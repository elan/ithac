<?php

namespace App\Manager;

use App\Entity\User;
use App\Manager\CacheManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class UserManager
{
    private $em;
    private $security;
    protected $cm;

    public function __construct(EntityManagerInterface $em, Security $security, CacheManager $cm)
    {
        $this->em = $em;
        $this->security = $security;
        $this->cm = $cm;
    }

    public function getUsers()
    {
        $usersCache = $this->cm->get("user.all");

        if ($usersCache->isHit()) {
            $users = $usersCache->get();
        } else {
            $users = $this->em->getRepository(User::class)->findAll();
            $this->cm->store($usersCache, $users);
        }

        return $users;
    }

    public function setRole(User $user, $roleToToggle)
    {
        $user->setRoles($roleToToggle);
        $this->saveUser($user);

        return;
    }

    public function create($mail, $password, $username, $passwordEncoder)
    {
        $user = new User();
        $user->setEmail($mail);
        $user->setUsername($username);
        $user->setLastAccess(new \DateTime());
        $user->setPassword($passwordEncoder->encodePassword($user, $password));

        $user = $this->saveUser($user);

        return $user;
    }

    public function getCurrentUser()
    {
        return $this->security->getUser();
    }

    public function anonymiseUsers()
    {
        $users = $this->em->getRepository(User::class)->getNonAnonymisedYet();
        foreach ($users as $user) {
            echo "Anonymisation userId: ".$user->getId()."\n";
            $this->anonymizeUserAccount($user, "full");
        }
        $this->em->flush();

        return;
    }

    public function anonymizeUserAccount(User $user)
    {
        $user->setEmail('anonymous-email-'.$user->getId().'@anonymous.fr');
        $user->setResetToken(null);
        $user->setLastAccess(null);
        $user->setAnonymous(true);
        $this->setRole($user, []);

        $this->saveUser($user);

        return;
    }

    public function saveUser(User $user, $clearCache = true)
    {
        $this->em->persist($user);
        $this->em->flush();
        if ($clearCache) {
            $this->cm->clear("user.all");
        }

        return $user;
    }
}
