<?php

namespace App\Manager;

use App\Entity\Paratext;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;

class VisualisationManager
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    public function extendedToNested($paratextsExtended) {
        $dates = [];
        foreach ($paratextsExtended as $paratext) {
            $dates[$paratext['date']][] = $paratext;
            ksort($dates);
        }

        $datesLocations = [];
        foreach ($dates as $date => $paratexts) {
            foreach ($paratexts as $paratext) {
                $datesLocations[$date][$paratext["location"]][] = $paratext;
            }
        }

        $datesLocationsAuthors = [];
        foreach ($datesLocations as $date => $locations) {
            foreach ($locations as $location => $paratexts) {
                foreach ($paratexts as $paratext) {
                    $datesLocationsAuthors[$date][$location][$paratext["author"]][] = $paratext;
                }
            }
        }

        $datesLocationsAuthorsTitles = [];
        foreach ($datesLocationsAuthors as $date => $locations) {
            foreach ($locations as $location => $authors) {
                foreach ($authors as $author => $paratexts) {
                    foreach ($paratexts as $paratext) {
                        $datesLocationsAuthorsTitles[$date][$location][$author][$paratext["title"]][] = $paratext;
                    }
                }
            }
        }

        $datesLocationsAuthorsTitlesAuthor = [];
        foreach ($datesLocationsAuthorsTitles as $date => $locations) {
            foreach ($locations as $location => $authors) {
                foreach ($authors as $author => $titles) {
                   foreach ($titles as $title => $paratexts) {
                    foreach ($paratexts as $paratext) {
                        $datesLocationsAuthorsTitlesAuthor[$date][$location][$author][$title][$paratext["author_paratext"]] = $paratext["filename"];
                    }
                   } 
                }
            }
        }

        return $datesLocationsAuthorsTitlesAuthor;
    }

    public function getParatextsExtended() {
        $paratexts = $this->em->getRepository(Paratext::class)->findAll();
        $paratextsExtended = [];
        foreach ($paratexts as $paratext) {

            $xml = file_get_contents("upload/tei/" . $paratext->getFilename());

            $crawler = new Crawler($xml);

            $language = $crawler->filterXPath('//TEI/teiHeader/fileDesc/titleStmt/title/@xml:lang');
            $pubPlace = $crawler->filterXPath('//TEI/teiHeader/fileDesc/sourceDesc/bibl/pubPlace');
            $anas = $crawler->filterXPath('//*/@ana');
            $pubPlace = $pubPlace->count() > 0 ? $pubPlace->text() : "";
            $language = $language->count() > 0 ? $language->text() : "";
            $anas = ($anas->count() > 0) ? $this->getDistinctAnas($anas) : [];

            $p["id"] = $paratext->getId();
            $p["filename"] = $paratext->getOriginalFilename();
            $p["title"] = preg_replace("/<br>|\n/", "", $paratext->getName());
            $p["location"] = $pubPlace;
            $p["lang"] = $language;
            $p["date"] = $paratext->getEdition()->getName();
            $p["author"] = $paratext->getEdition()->getAuthor()->getName();
            $p["author_paratext"] = $paratext->getAuthor();
            $p["semantic"] = $anas;

            $paratextsExtended[] = $p;
        }

        return $paratextsExtended;
    }

    private function getDistinctAnas($anaEl)
    {
        $out = [];
        $toExclude = ["#AR", "#G", "#R"];

        $anaEl->each(function (Crawler $el) use (&$out, $toExclude) {
            $anas = explode(" ", $el->text());
            foreach ($anas as $ana) {
                if (!in_array($ana, $out) && $ana != "" && !in_array($ana, $toExclude))
                    $out[] = $ana;
            }
        });

        return $out;
    }

}
