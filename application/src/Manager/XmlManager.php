<?php

namespace App\Manager;

use App\Entity\Paratext;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class XmlManager
{
    private $dir;
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
        $this->dir = $this->params->get('xml_directory');
    }

    public function getOdtContent(Paratext $paratext)
    {
        $xslFile = "../data/xsl/tei2odt/tei2odt_content.xsl";
        $xsl = new \DOMDocument();
        $xsl->load($xslFile);

        $xslt = new \XSLTProcessor();
        $xslt->importStylesheet($xsl);

        $xmlFile = $this->dir . "/" . $paratext->getFilename();
        $xml = new \DOMDocument();
        $xml->load($xmlFile);
        $xml->xinclude(LIBXML_NOWARNING);

        return $xslt->transformToXml($xml);
    }


    public function getHTML(Paratext $paratext)
    {
        $xslFile = "../data/xsl/tei2html.xsl";
        $xsl = new \DOMDocument();
        $xsl->load($xslFile);

        $xslt = new \XSLTProcessor();
        $xslt->importStylesheet($xsl);

        $xmlFile = $this->dir . "/" . $paratext->getFilename();
        $xml = new \DOMDocument();
        $xml->load($xmlFile);
        $xml->xinclude(LIBXML_NOWARNING);

        return $xslt->transformToXML($xml);
    }
    public function getParatextPath(Paratext $paratext)
    {
        return $this->dir . "/" . $paratext->getFilename();
    }

    public function getPublicHTML(Paratext $paratext)
    {
        $xslFile = "../data/xsl/tei2montroir.xsl";
        $xsl = new \DOMDocument();
        $xsl->load($xslFile);

        $xslt = new \XSLTProcessor();
        $xslt->importStylesheet($xsl);

        $xmlFile = $this->dir . "/" . $paratext->getFilename();
        $xml = new \DOMDocument();
        $xml->load($xmlFile);
        $xml->xinclude(LIBXML_NOWARNING);

        return $xslt->transformToXML($xml);
    }
}
