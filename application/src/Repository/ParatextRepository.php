<?php

namespace App\Repository;

use App\Entity\Paratext;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Paratext|null find($id, $lockMode = null, $lockVersion = null)
 * @method Paratext|null findOneBy(array $criteria, array $orderBy = null)
 * @method Paratext[]    findAll()
 * @method Paratext[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParatextRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Paratext::class);
    }
}
