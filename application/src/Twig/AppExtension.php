<?php
namespace App\Twig;

use App\Entity\Parameters;
use App\Entity\Paratext;
use App\Manager\CacheManager;
use App\Manager\XmlManager;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private $em;
    private $cm;
    private $xm;

    public function __construct(EntityManagerInterface $em, CacheManager $cm, XmlManager $xm)
    {
        $this->em = $em;
        $this->cm = $cm;
        $this->xm = $xm;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getParameters', [$this, 'getParameters']),
            new TwigFunction('customSort', [$this, 'customSort']),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('paratextToHtml', [$this, 'paratextToHtml']),
            new TwigFilter('publicParatextToHtml', [$this, 'publicParatextToHtml']),
        ];
    }

    public function getParameters()
    {
        $parametersCache = $this->cm->get("parameters");

        if ($parametersCache->isHit()) {
            $parameters = $parametersCache->get();
        } else {
            $parameters = $this->em->getRepository(Parameters::class)->findOneBy([]);
            $this->cm->store($parametersCache, $parameters);
        }

        return $parameters;
    }

    public function paratextToHtml(Paratext $paratext)
    {
        return $this->xm->getHTML($paratext);
    }

    public function publicParatextToHtml(Paratext $paratext)
    {
        return $this->xm->getPublicHTML($paratext);
    }

    public function customSort($paratexts)
    {
        $paratexts = $paratexts->toArray();
       
        usort($paratexts, function (Paratext $a, Paratext $b): int {
            $paratextNameA = $a->getOriginalFilename();
            $paratextNameB = $b->getOriginalFilename();

            preg_match('/(.*)_p(.*)/', $paratextNameA, $matchesA);
            preg_match('/(.*)_p(.*)/', $paratextNameB, $matchesB);

            $firstPartA = $matchesA[1];
            $firstPartB = $matchesB[1];

            $secondPartA = $matchesA[2];
            $secondPartB = $matchesB[2];

            if ($firstPartA === $firstPartB) {
                return $secondPartA <=> $secondPartB;
            }
            return $firstPartA <=> $firstPartB;
        });

        return $paratexts;
    }
}
